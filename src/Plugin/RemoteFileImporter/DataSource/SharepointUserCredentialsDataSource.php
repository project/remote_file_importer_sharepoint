<?php

namespace Drupal\remote_file_importer_sharepoint\Plugin\RemoteFileImporter\Datasource;

use Drupal\Core\File\FileSystemInterface;
use Drupal\remote_file_importer\DataSourcePluginBase;
use Office365\Runtime\Auth\UserCredentials;
use Office365\SharePoint\ClientContext;

/**
 * Provids file import functionality for sharepoint.
 *
 * @RemoteFileImporterDataSourcePlugin(
 *   id = "rif_sharepont_user_credentials_datasource",
 *   label = @Translation("Sharepoint with user credentials"),
 * )
 */
class SharepointUserCredentialsDataSource extends DataSourcePluginBase {

  /**
   * Root folder on sharepoint.
   *
   * @var Office365\SharePoint\Folder
   */
  private $rootFolder;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm($values = []) {

    $form = [];
    $form['host'] = [
      '#type' => 'textfield',
      '#default_value' => $values['host'] ?? '',
      '#title' => $this->t('Host'),
      '#required' => TRUE,
    ];

    $form['user'] = [
      '#type' => 'textfield',
      '#default_value' => $values['user'] ?? '',
      '#title' => $this->t('User'),
      '#required' => TRUE,
    ];

    $form['password'] = [
      '#type' => 'password',
      '#default_value' => $values['password'] ?? '',
      '#title' => $this->t('Password'),
      '#required' => TRUE,
    ];

    $form['remote_dir'] = [
      '#type' => 'textfield',
      '#default_value' => $values['remote_dir'] ?? '',
      '#title' => $this->t('Remote Dir'),
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function establishConnection($settings) {

    $credentials = new UserCredentials($settings['user'], $settings['password']);
    $this->connection = (new ClientContext($settings['host']))->withCredentials($credentials);
    $this->rootFolder = $this->getFolder($settings['remote_dir']);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function collectFiles() {
    $files = [];
    $this->recursiveFolderScan($this->rootFolder, $files);
    return $files;
  }

  /**
   * Recursively scans folders.
   */
  private function recursiveFolderScan($folder, &$files) {
    foreach ($folder->getFiles()->get()->executeQuery() as $file) {
      // Ignore system files.
      if ($file->getAuthor()->get()->executeQuery()->getProperty('UserPrincipalName')) {
        $files[] = [
          'path' => str_replace($this->rootFolder->getServerRelativeUrl(), "", $file->getProperty("ServerRelativeUrl")),
          'metadata' => [
            'modified_date' => strtotime($file->getProperty("TimeLastModified")),
          ],
        ];
      }
    }
    foreach ($folder->getFolders()->get()->executeQuery() as $folder) {
      $this->recursiveFolderScan($folder, $files);
    }
  }

  /**
   * Load folder.
   */
  private function getFolder($relativePath, $folder = NULL) {
    if (!$folder) {
      $folder = $this->connection->getWeb();
    }
    foreach (explode('/', $relativePath) as $dir) {
      if ($dir) {
        $folder = $folder->getFolders()->getByUrl($dir)->get()->executeQuery();
      }
    }
    return $folder;
  }

  /**
   * {@inheritdoc}
   */
  public function importFile($localFileName, $remoteFileName) {

    $folder = $this->getFolder(trim(dirname($remoteFileName), '/'), $this->rootFolder);
    $file = $folder->getFiles()->get()->executeQuery()->getByUrl(basename($remoteFileName))->get()->executeQuery();
    $data = $file->openBinary($this->connection, $file->getServerRelativeUrl());
    \Drupal::service('file.repository')->writeData($data, $localFileName, FileSystemInterface::EXISTS_REPLACE);
  }

  /**
   * {@inheritdoc}
   */
  public function closeConnection() {
    return TRUE;
  }

}
